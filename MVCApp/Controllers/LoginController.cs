﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.DirectoryServices;
using System.Web.Security;
using System.Web.Script.Serialization;
using MVCApp.Models;
using System.Text;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;

namespace MVCApp.Controllers
{
    public class LoginController : Controller
    {

        DBContext db = new DBContext();

        public ActionResult Index()
        {
            var customUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.FullName = "";

            //if (customUser.EmpID > 0)
            if (customUser != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult SignIn()
        {
            Login loginModel = new Login();

            if (Request.Cookies["Login"] != null)
            {
                loginModel.Username = Request.Cookies["Login"]["usr"].ToString();
                loginModel.Password = Request.Cookies["Login"]["pwd"].ToString();
                //loginModel.Group = Convert.ToInt32(Request.Cookies["Login"]["grp"]);

                loginModel.RememberMe = true;
            }
            return PartialView(loginModel);
        }


        public bool UpdateAdminUser(string fullName, int userID)
        {
            bool isSuccess = false;
            string returnMessage = "";
  
            var result = db.Database.SqlQuery<SendSurveyResponse>("sp_CSATUpdateAdminUser @fullName,@userId",
                     new SqlParameter("fullName", fullName),
                     new SqlParameter("userId", userID)
                     ).FirstOrDefault();

            return true;
        }

        public Tuple<int, int, int, string> getUserID(string userName)
        {
            int userID = 0;
            int roleID = 0;
            int groupID = 0;
            string fullName = "";

            var _list = db.Database.SqlQuery<SendSurveyAdmins>("sp_ListCSATUsers").ToList();

            userID = _list.Any(i => i.Email == userName) ? _list.FirstOrDefault(i => i.Email == userName).UserID : 0;
            roleID = _list.Any(i => i.Email == userName) ? _list.FirstOrDefault(i => i.Email == userName).isAdmin == true ? 1 : 0 : 0;
            groupID = _list.Any(i => i.Email == userName) ? _list.FirstOrDefault(i => i.Email == userName).GroupID : 0;
            fullName = _list.Any(i => i.Email == userName) ? _list.FirstOrDefault(i => i.Email == userName).FullName : "";

            var tup = new Tuple<int, int, int, string>(userID, roleID, groupID, fullName);

            return tup;
        }
        public void SaveImage(Image image, string userName)
        {
            string _filePath = "~/Resources/Avatars/" + userName + ".png";

            string path = System.Web.Hosting.HostingEnvironment.MapPath(_filePath);

            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            image.Save(path);
            image.Dispose();
        }
        public Image ConvertByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream stream = new MemoryStream(byteArrayIn);
            return Bitmap.FromStream(stream);
        }

        [HttpPost]
        public JsonResult Login(Login loginModel)
        {
            if (ModelState.IsValid)
            {
                string _username = loginModel.Username;
                string _userpassword = loginModel.Password;
                int _group = loginModel.Group;

                //string inputUser = _username;
                //string[] sliceUsername = _username.Split("-".ToCharArray());
                //_username = sliceUsername[0];
                //string[] checkDomain = _username.Split("\\".ToString().ToCharArray());

                //if (checkDomain.Length > 1)
                //    _username = checkDomain[1].ToString();

                bool isLogged = false;
                string path = "LDAP://DC=emrsn,DC=org";
                string filterAttributes = "";
                string emailAdd = "";
                string businessUnit = "";
                string location = "";
                string name = "";
                string fullUserName = "EMRSN" + @"\" + _username;
                string emailDisplayName = "";
                string avatarPath = "";
                string title = "";

                DirectoryEntry de = new DirectoryEntry(path, _username, _userpassword);
                //DirectoryEntry de = new DirectoryEntry(path);

                try
                {
                    Object obj = de.NativeObject;
                    DirectorySearcher ds = new DirectorySearcher(de);
                    //ds.Filter = "(SAMAccountName=" + _username + ")";
                    ds.Filter = "(mail=" + _username + ")";

                    ds.PropertiesToLoad.Add("cn");
                    ds.PropertiesToLoad.Add("mail");
                    ds.PropertiesToLoad.Add("company");
                    ds.PropertiesToLoad.Add("co");
                    ds.PropertiesToLoad.Add("givenName");
                    ds.PropertiesToLoad.Add("SN");
                    ds.PropertiesToLoad.Add("thumbnailPhoto");
                    ds.PropertiesToLoad.Add("title");

                    SearchResult result = ds.FindOne();

                    if (result != null)
                    {
                        path = result.Path;
                        filterAttributes = (String)result.Properties["cn"][0];
                        emailAdd = (String)result.Properties["mail"][0];
                        businessUnit = (String)result.Properties["company"][0];
                        location = (String)result.Properties["co"][0];
                        name = (String)result.Properties["givenName"][0];
                        emailDisplayName = (String)result.Properties["givenName"][0] + " " + (String)result.Properties["SN"][0];
                        avatarPath = string.Format("{0}.png", _username);
                        title = (String)result.Properties["title"][0];

                        try
                        {
                            byte[] data = result.Properties["thumbnailPhoto"][0] as byte[];
                            if (data != null)
                            {
                                Image img = ConvertByteArrayToImage(data);
                                SaveImage(img, _username);
                            }
                            else
                            {
                                StringBuilder str = new StringBuilder();

                                foreach (PropertyValueCollection p in result.Properties)
                                    str.AppendLine(string.Format("{0} : {1}", p.PropertyName, p.Value));

                                string lastString = str.ToString();
                            }
                        }
                        catch {
                            avatarPath = "default.png";
                        }

                        isLogged = true;
                    }

                    var userDetails = getUserID(_username.ToUpper());

                    if (userDetails.Item1 > 0)
                    {
                        if (isLogged == true)
                        {
                            CustomPrincipalSerializeModel serializerUser = new CustomPrincipalSerializeModel();
                            
                            serializerUser.Username = _username;
                            serializerUser.EmailAddress = emailAdd;
                            serializerUser.RoleID = userDetails.Item2;
                            serializerUser.FullName = emailDisplayName;
                            serializerUser.GroupID = userDetails.Item3;
                            serializerUser.Avatar = avatarPath;
                            serializerUser.Title = title;
                            serializerUser.EmpID = userDetails.Item1;

                            if (userDetails.Item4 == null)
                                UpdateAdminUser(emailDisplayName, userDetails.Item1);

                            JavaScriptSerializer serializer = new JavaScriptSerializer();

                            string userData = serializer.Serialize(serializerUser);

                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                1,
                                name,
                                DateTime.Now,
                                DateTime.Now.AddMinutes(180),
                                true,
                                userData,
                                FormsAuthentication.FormsCookiePath);

                            string encTicket = FormsAuthentication.Encrypt(authTicket);
                            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                            faCookie.Expires = authTicket.Expiration;
                            Response.Cookies.Add(faCookie);

                   //
                            loginModel.IsSuccess = true;
                            loginModel.ErrorMessage = "";

                        }
                        else
                        {
                            loginModel.IsSuccess = false;
                            loginModel.ErrorMessage = "Cant find user " + _username;
                        }
                    }
                    else
                    {
                        loginModel.IsSuccess = false;
                        loginModel.ErrorMessage = "User is not registered";
                    }
                }
                catch (Exception ex)
                {
                    loginModel.IsSuccess = false;
                    loginModel.ErrorMessage = "Your Active Directory Account Validation failed. Please try again by re-typing all your credentials. " + ex.Message;
                }
            }

            return Json(loginModel);
        }


        public void registerUser(CustomPrincipalSerializeModel model)
        {

        }

    }
}