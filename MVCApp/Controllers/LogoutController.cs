﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcApp.Controllers
{
    public class LogoutController : Controller
    {
        public ActionResult Index()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            try
            {
                HttpCookie sessionCokie = HttpContext.Request.Cookies["_CurrentSessionId"];
                if (sessionCokie != null)
                {
                    HttpCookie nCookie = new HttpCookie("_CurrentSessionId");
                    nCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(nCookie);
                }
            }
            catch { }
            return RedirectToAction("Index", "Login");
        }

    }
}
