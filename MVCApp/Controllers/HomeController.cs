﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using System.Drawing;
using System.IO;

using SpreadsheetLight;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Net.Mail;
using System.Data;
using Newtonsoft.Json;
using MVCApp.Models;
using System.Data.SqlClient;
using System.Globalization;
using System.Collections;
using System.Net;
//using Microsoft.Office.Interop.Outlook;
//using System.Text;

namespace MVCApp.Controllers
{

    //[CustomAuthorize]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {
        DBContext db = new DBContext();

        public ActionResult Index()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.Now = DateTime.Now;
            //List<EmailFromOutlook> list = readEmails().Where(i=>i.SubjectEmail == "FW: [EXTERNAL] Unsubscribe - CSAT/CE Survey").ToList();

            //ViewBag.GetUnsubscribedCustomer = list.Count();

            return View();
            //}
        }

        public ActionResult EditGroupInfoFormPartial()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_EditGroupInfo");

        }

        public PartialViewResult GenerateServiceRecoveryReport()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_GenerateServiceRecoveryReport");
        }

        public PartialViewResult ServiceRecovery()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_ServiceRecovery");
        }

        public PartialViewResult EditServiceRecovery(int serviceRecoveryID)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            ServiceRecoveryModel model = db.Database.SqlQuery<ServiceRecoveryModel>("sp_CSAT_GetServiceRecovery @serviceRecoveryID",
                    new SqlParameter("serviceRecoveryID", serviceRecoveryID)
                    ).FirstOrDefault();

            return PartialView("_EditServiceRecovery", model);
        }

        public PartialViewResult ServiceRecoveryListPartial()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.Now = DateTime.Now;

            var list = db.Database.SqlQuery<ServiceRecoveryList>("sp_CSAT_ListServiceRecovery @userID", new SqlParameter("userID", currentUser.UserID)).ToList();

            //var list = ListUserAdmins().Where(i=>i.isAdmin == true).ToList();

            return PartialView("_ServiceRecoveryList", list);
        }


        [HttpPost]
        public JsonResult AddServiceRecovery(ServiceRecovery model)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            var currentUserID = currentUser.UserID;
            SendSurveyResponse result = new SendSurveyResponse();

            result = db.Database.SqlQuery<SendSurveyResponse>("sp_CSAT_AddServiceRecoveryV2 @DateReceived, @Customer,@ContactName,@Detractor,@IssueRaised,@IsApprovedCallBackService,@IsServiceRecoveryAttemptMade,@DateOfContact, @ContactedBy,@SuccessfulAttemptToContact,@IsThereEffort, @Remarks,@ServiceRecoveryWithinTarget,@AddedByID,@ResolutionID",
                    new SqlParameter("DateReceived", model.dateReceived),
                    new SqlParameter("Customer", model.customer),
                    new SqlParameter("ContactName", model.contactName),
                    new SqlParameter("Detractor", model.detractor),
                    new SqlParameter("IssueRaised", model.issueRaised),
                    new SqlParameter("IsApprovedCallBackService", model.isApprovedCallBackService),
                    new SqlParameter("IsServiceRecoveryAttemptMade", model.isServiceRecoveryAttemptMade),
                    new SqlParameter("DateOfContact", model.DateOfContact == null ? "" : model.DateOfContact),
                    new SqlParameter("ContactedBy", model.contactedBy == null ? "" : model.contactedBy),
                    new SqlParameter("SuccessfulAttemptToContact", model.isSuccessfulAttemptContact),
                    new SqlParameter("IsThereEffort", model.isThereEffort),
                    new SqlParameter("Remarks", model.Remarks),
                    new SqlParameter("ServiceRecoveryWithinTarget", model.isServiceRecoveryWithinTarget),
                    new SqlParameter("AddedByID", currentUserID),
                    new SqlParameter("ResolutionID", model.ResolutionID)

                    ).FirstOrDefault();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult UpdateServiceRecovery(ServiceRecovery model)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            var currentUserID = currentUser.UserID;
            SendSurveyResponse result = new SendSurveyResponse();

            result = db.Database.SqlQuery<SendSurveyResponse>("sp_CSAT_UpdateServiceRecovery @DateReceived, @Customer,@ContactName,@Detractor,@IssueRaised,@IsApprovedCallBackService,@IsServiceRecoveryAttemptMade,@DateOfContact, @ContactedBy,@SuccessfulAttemptToContact,@IsThereEffort, @Remarks,@ServiceRecoveryWithinTarget,@AddedByID,@ServiceRecoveryID,@ResolutionID",
                    new SqlParameter("DateReceived", model.dateReceived),
                    new SqlParameter("Customer", model.customer),
                    new SqlParameter("ContactName", model.contactName),
                    new SqlParameter("Detractor", model.detractor),
                    new SqlParameter("IssueRaised", model.issueRaised),
                    new SqlParameter("IsApprovedCallBackService", model.isApprovedCallBackService),
                    new SqlParameter("IsServiceRecoveryAttemptMade", model.isServiceRecoveryAttemptMade),
                    new SqlParameter("DateOfContact", model.DateOfContact == null ? "" : model.DateOfContact),
                    new SqlParameter("ContactedBy", model.contactedBy == null ? "" : model.contactedBy),
                    new SqlParameter("SuccessfulAttemptToContact", model.isSuccessfulAttemptContact),
                    new SqlParameter("IsThereEffort", model.isThereEffort),
                    new SqlParameter("Remarks", model.Remarks),
                    new SqlParameter("ServiceRecoveryWithinTarget", model.isServiceRecoveryWithinTarget),
                    new SqlParameter("AddedByID", currentUserID),
                    new SqlParameter("@ServiceRecoveryID", model.ID),
                    new SqlParameter("ResolutionID", model.ResolutionID)

                    ).FirstOrDefault();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteServiceRecovery(int id)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            var currentUserID = currentUser.UserID;
            SendSurveyResponse result = new SendSurveyResponse();

            result = db.Database.SqlQuery<SendSurveyResponse>("sp_CSAT_DeleteServiceRecovery @serviceRecoveryID, @userID",
                    new SqlParameter("serviceRecoveryID", id),
                    new SqlParameter("userID", currentUserID)
                    ).FirstOrDefault();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult UpdateUserGroupInfo(int userID, int groupID)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            var currentUserID = currentUser.UserID;

            var result = db.Database.SqlQuery<SendSurveyResponse>("sp_CSAT_UpdateUserGroupInfo @userID , @groupID , @updatedByID ",
                     new SqlParameter("userID", userID),
                     new SqlParameter("groupID", groupID),
                     new SqlParameter("updatedByID", currentUserID)
                     ).FirstOrDefault();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendSurvey()
        {
            return View();
        }
        public PartialViewResult ChangeGroup()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.Now = DateTime.Now;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_ChangeGroup");
        }

        public PartialViewResult AddUserAdmin()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.Now = DateTime.Now;

            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_AddUser");
        }

        [HttpPost]
        public JsonResult manageAdminUser(int action, int userID)
        {
            bool isSuccess = false;
            string returnMessage = "";
            var currentUser = HttpContext.User as Custom2IPrincipal;
            var currentUserID = currentUser.UserID;

            var result = db.Database.SqlQuery<SendSurveyResponse>("sp_CSATManageUserAdmin @action , @userID , @updatedByID ",
                     new SqlParameter("action", action),
                     new SqlParameter("userID", userID),
                     new SqlParameter("updatedByID", currentUserID)
                     ).FirstOrDefault();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveAdminUser(string emailAddress, bool isAdmin, int groupID)
        {
            bool isSuccess = false;
            string returnMessage = "";
            Helpers.DirectorySearcher ds = new Helpers.DirectorySearcher();
            var userInfo = ds.GetADInformation(emailAddress);

            if (userInfo.Item2 == "")
            {
                //List<SendSurveyResponse> ssr = new List<SendSurveyResponse>();
                SendSurveyResponse ssr = new SendSurveyResponse { isSuccess = 0, Message = "Invalid Email" };

                return Json(new { data = ssr }, JsonRequestBehavior.AllowGet);
            }

            var currentUser = HttpContext.User as Custom2IPrincipal;
            var userID = currentUser.UserID;

            var result = db.Database.SqlQuery<SendSurveyResponse>("sp_AddAdminUser @emailAddress, @isAdmin, @userName, @fullName,@groupID , @addedByID",
                     new SqlParameter("emailAddress", emailAddress),
                     new SqlParameter("isAdmin", isAdmin),
                     new SqlParameter("userName", userInfo.Item2),
                     new SqlParameter("fullName", userInfo.Item1),
                     new SqlParameter("groupID", groupID),
                     new SqlParameter("addedByID", userID)
                     ).FirstOrDefault();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AddSurveyPartial()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            ViewBag.Now = DateTime.Now;
            return PartialView("_AddSurvey");
        }

        public PartialViewResult AdminListPartial(string emailAddress)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.Now = DateTime.Now;

            var list = emailAddress == "" || emailAddress == null ? ListUserAdmins().Where(i => i.isAdmin == true).ToList() : ListUserAdmins().Where(i => i.isAdmin == true && i.Email.Contains(emailAddress.ToUpper())).ToList();

            //var list = ListUserAdmins().Where(i=>i.isAdmin == true).ToList();

            return PartialView("_AdminList", list);
        }

        public PartialViewResult UserListPartial(string emailAddress)
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.Now = DateTime.Now;

            var list = emailAddress == "" || emailAddress == null ? ListUserAdmins().Where(i => i.isAdmin == false).ToList() : ListUserAdmins().Where(i => i.isAdmin == false && i.Email.Contains(emailAddress.ToUpper())).ToList();

            return PartialView("_UserList", list);
        }

        [HttpPost]
        public JsonResult SaveSurvey(string customerEmail, string subjLine, bool sendImmediately)
        {
            //sp_AddSendSurvey
            var currentUser = HttpContext.User as Custom2IPrincipal;
            SendSurveyResponse result = new SendSurveyResponse();
            MailMaster mail = new MailMaster();
            string message = "";
            string headerPath1 = "";
            string headerPath2 = "";
            string responseMessage = "";

            var dateSent = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time");

            bool isSuccess = false;
            try
            {

                result = db.Database.SqlQuery<SendSurveyResponse>("sp_AddSendSurvey @UserName, @UserEmail, @CustomerEmail, @SubjectLine, @SendImmediately, @SendByTeamId, @DateSent",
                       new SqlParameter("UserName", currentUser.FullName),
                       new SqlParameter("UserEmail", currentUser.EmailAddress),
                       new SqlParameter("CustomerEmail", customerEmail),
                       new SqlParameter("SubjectLine", subjLine),
                       new SqlParameter("SendImmediately", sendImmediately),
                       new SqlParameter("SendByTeamId", currentUser.GroupID),
                       new SqlParameter("DateSent", dateSent)
                       ).FirstOrDefault();

                if (result.isSuccess == 1 && sendImmediately == true)
                {
                    if (currentUser.GroupID == 1)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/InsideSales/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/InsideSales/InsideSales1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/InsideSales/InsideSales2.png");
                    }
                    else if (currentUser.GroupID == 2)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/CS/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/CS/CustomerService1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/CS/CustomerService2.jpg");
                    }
                    else if (currentUser.GroupID == 3)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/Lumity/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/Lumity/Picture1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/Lumity/Picture2.jpg");
                    }
                    else if (currentUser.GroupID == 5)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/TechnicalSupport/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/TechnicalSupport/Picture1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/TechnicalSupport/Picture2.jpg");
                    }
                    else if (currentUser.GroupID == 6)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/CustomerSuccessDataAnalytics/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture2.jpg");
                    }
                    else if (currentUser.GroupID == 7)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/CustomerSuccessDataAnalytics/imageHtmlv2_DA.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture2.jpg");
                    }
                    else if (currentUser.GroupID == 8)
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/FieldProjectServices/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/FieldProjectServices/Picture1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/FieldProjectServices/Picture2.jpg");
                    }
                    else
                    {
                        message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/TechnicalServices/imageHtmlv2.html"));
                        headerPath1 = Server.MapPath("~/content/EmailTemplates/TechnicalServices/Picture1.jpg");
                        headerPath2 = Server.MapPath("~/content/EmailTemplates/TechnicalServices/Picture2.jpg");
                    }

                    mail.sendSurveyEmail(customerEmail, message, subjLine, headerPath1, headerPath2);
                }

                isSuccess = true;
                responseMessage = "Successfully Sent";
            }
            catch (System.Exception ex)
            {
                isSuccess = false;
                responseMessage = "Error sending email, please screen shot this and send to IT Support : " + ex.Message;
            }

            return Json(new { data = result, isSuccess = isSuccess, response = responseMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SendAllSavedEmails(DateTime? dateSelected)
        {
            List<SendSurveyViewModel> List = new List<Models.SendSurveyViewModel>();
            MailMaster mail = new MailMaster();

            //DateTime dateSelected = DateTime.Now;
            dateSelected = dateSelected == null ? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time") : dateSelected;

            bool isSuccess = false;
            string returnMessage = "";
            string message = "";
            string headerPath1 = "";
            string headerPath2 = "";

            try
            {
                List = ListSurveys(dateSelected).Where(i => i.Status == "Saved" && i.isSubscribed != "No").ToList();
                var currentUser = HttpContext.User as Custom2IPrincipal;

                if (currentUser.GroupID == 1)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/InsideSales/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/InsideSales/InsideSales1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/InsideSales/InsideSales2.jpg");

                }
                else if (currentUser.GroupID == 2)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/CS/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/CS/CustomerService1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/CS/CustomerService2.jpg");
                }
                else if (currentUser.GroupID == 3)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/Lumity/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/Lumity/Picture1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/Lumity/Picture2.jpg");
                }
                else if (currentUser.GroupID == 5)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/TechnicalSupport/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/TechnicalSupport/Picture1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/TechnicalSupport/Picture2.jpg");
                }
                else if (currentUser.GroupID == 6)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/CustomerSuccessDataAnalytics/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture2.jpg");
                }
                else if (currentUser.GroupID == 7)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/CustomerSuccessDataAnalytics/imageHtmlv2_DA.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/CustomerSuccessDataAnalytics/Picture2.jpg");
                }
                else if (currentUser.GroupID == 8)
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/FieldProjectServices/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/FieldProjectServices/Picture1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/FieldProjectServices/Picture2.jpg");
                }
                else
                {
                    message = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplates/TechnicalServices/imageHtmlv2.html"));
                    headerPath1 = Server.MapPath("~/content/EmailTemplates/TechnicalServices/Picture1.jpg");
                    headerPath2 = Server.MapPath("~/content/EmailTemplates/TechnicalServices/Picture2.jpg");
                }

                int counter = 0;

                foreach (var sur in List)
                {
                    
                    if (updateSurveyToSent(sur.SurveyID) > 0)
                    {
                        mail.sendSurveyEmail(sur.CustomerEmail, message, sur.SubjectLine, headerPath1, headerPath2);
                        counter = counter + 1;
                    }
                }

                if (List.Count == 0)
                    returnMessage = "Nothing to Send";
                else
                    returnMessage = string.Format("Sent {0} Survey(s)", counter);

                isSuccess = true;
            }
            catch (System.Exception ex)
            {
                isSuccess = false;
                returnMessage = ex.Message;
            }
            return Json(new { data = isSuccess, message = returnMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateUserGroup(int userID, int selectedGroup)
        {
            //sp_AddSendSurvey
            var currentUser = HttpContext.User as Custom2IPrincipal;
            SendSurveyResponse result = new SendSurveyResponse();
            MailMaster mail = new MailMaster();
            string message = "";
            string headerPath1 = "";
            string headerPath2 = "";
            string responseMessage = "";

            bool isSuccess = false;

            try
            {
                var returnId = updateUserGroup(userID, selectedGroup);

                isSuccess = true;
                responseMessage = "Successfully Updated";
            }
            catch (System.Exception ex)
            {
                isSuccess = false;
                responseMessage = "Error Updating Group : " + ex.Message;
            }

            return Json(new { data = result, isSuccess = isSuccess, response = responseMessage }, JsonRequestBehavior.AllowGet);
        }

        private long updateSurveyToSent(int surveyID)
        {
            long returnID = 0;

            try
            {
                var dateSent = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time");
                ArrayList paramList = new ArrayList();
                paramList.Add(new SqlParameter("@surveyID", surveyID));
                paramList.Add(new SqlParameter("@dateSent", dateSent));

                //returnID = db.Database.ExecuteSqlCommand("sp_UpdateSendSurvey @surveyID, @dateSent", new SqlParameter("@surveyID",surveyID), new SqlParameter("@dateSent", dateSent));
                returnID = executeCommandBySP("sp_UpdateSendSurvey", paramList, true);
            }
            catch (System.Exception ex)
            {
                returnID = 0;
            }

            return returnID;
        }

        private long updateUserGroup(int userID, int selectedGroup)
        {
            long returnID = 0;

            try
            {
                ArrayList paramList = new ArrayList();
                paramList.Add(new SqlParameter("@userID", userID));
                paramList.Add(new SqlParameter("@selectedGroup", selectedGroup));

                returnID = executeCommandBySP("sp_CSATUpdateUser", paramList, false);
            }
            catch (System.Exception ex)
            {
                returnID = 0;
            }

            return returnID;
        }
        private List<SendSurveyViewModel> ListSurveys(DateTime? dateSelected)
        {
            List<SendSurveyViewModel> List = new List<Models.SendSurveyViewModel>();
            var currentUser = HttpContext.User as Custom2IPrincipal;
            DateTime dateSelectedEST = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateSelected.Value, "Eastern Standard Time");

            //List = db.Database.SqlQuery<SendSurveyViewModel>("sp_ListSendSurvey @Username, @DateAdded, @GroupID, @DateSent",
            //        new SqlParameter("@UserName", currentUser.FullName),
            //        new SqlParameter("@DateAdded", dateSelected),
            //        new SqlParameter("@GroupID", currentUser.RoleID),
            //         new SqlParameter("@DateSent", dateSelectedEST)).ToList();


            List = db.Database.SqlQuery<SendSurveyViewModel>("sp_ListSendSurveyV2 @UserEmailAddress, @DateAdded, @GroupID",
                    new SqlParameter("@UserEmailAddress", currentUser.EmailAddress),
                    new SqlParameter("@DateAdded", dateSelected.Value),
                    new SqlParameter("@GroupID", currentUser.GroupID)).ToList();

            return List;
        }

        public PartialViewResult ListSurveyPartial(DateTime? dateSelected)
        {
            //dateSelected = dateSelected == null ? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time") : dateSelected;
            dateSelected = dateSelected == null ? DateTime.Now : dateSelected;

            List<SendSurveyViewModel> List = new List<Models.SendSurveyViewModel>();

            List = ListSurveys(dateSelected);

            var currentUser = HttpContext.User as Custom2IPrincipal;
            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Now = DateTime.Now;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;

            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_SendSurveyList", List);
        }

        public PartialViewResult ListSurveySubscription(string customerSelected)
        {

            List<UnsubscribedCustomerViewModel> List = new List<Models.UnsubscribedCustomerViewModel>();
            var currentUser = HttpContext.User as Custom2IPrincipal;
            customerSelected = customerSelected == null ? "" : customerSelected;

            List = db.Database.SqlQuery<UnsubscribedCustomerViewModel>("sp_ListSendSurveyUnsubscription @customerEmail", new SqlParameter("customerEmail", customerSelected)).ToList();

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_UnsubscribtionList", List);
        }

        public PartialViewResult UnsubscribedCustomer()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;
            List<UnsubscribedResponse> List = new List<UnsubscribedResponse>();

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_AddUnsubscription");
        }

        public PartialViewResult GenerateReport()
        {
            var currentUser = HttpContext.User as Custom2IPrincipal;

            ViewBag.RoleID = currentUser.RoleID;
            ViewBag.UserID = currentUser.UserID;
            ViewBag.UserName = currentUser.Username;
            ViewBag.EmailAddress = currentUser.EmailAddress.ToLower();
            ViewBag.FullName = currentUser.FullName;
            ViewBag.Now = DateTime.Now;
            ViewBag.GroupID = currentUser.GroupID;
            ViewBag.Avatar = currentUser.Avatar;
            ViewBag.Title = currentUser.Title;
            ViewBag.ModalHeader = currentUser.GroupID == 2 ? "../Content/EmailTemplates/CS/CustomerService1.jpg" :
                                   currentUser.GroupID == 8 ? "../Content/EmailTemplates/FieldProjectServices/Picture1.jpg" :
                                   currentUser.GroupID == 1 ? "../Content/EmailTemplates/InsideSales/InsideSales1.jpg" :
                                   currentUser.GroupID == 3 ? "../Content/EmailTemplates/Lumity/Picture1.jpg" :
                                   currentUser.GroupID == 6 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    currentUser.GroupID == 7 ? "../Content/EmailTemplates/CustomerSuccessDataAnalytics/Picture1.jpg" :
                                    "../Content/EmailTemplates/TechnicalServices/Picture1.jpg";

            return PartialView("_GenerateReport");
        }

        [HttpPost]
        public JsonResult UnsubscribedToSurvey(string customerName, string customerEmail)
        {
            //sp_UnsubscribedSendSurvey

            UnsubscribedResponse List = new UnsubscribedResponse();

            var currentUser = HttpContext.User as Custom2IPrincipal;

            List = db.Database.SqlQuery<UnsubscribedResponse>("sp_UnsubscribedSendSurvey @UserName,@UserEmail,@SentByTeam,@customerName,@customerEmail ",
                            new SqlParameter("UserName", currentUser.FullName),
                            new SqlParameter("UserEmail", currentUser.EmailAddress),
                            new SqlParameter("SentByTeam", currentUser.GroupID),
                            new SqlParameter("customerName", customerName),
                            new SqlParameter("customerEmail", customerEmail)).FirstOrDefault();

            return Json(new { data = List }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult SubscribedToSurvey(int subscriptionID)
        {
            //sp_UnsubscribedSendSurvey

            UnsubscribedResponse List = new UnsubscribedResponse();

            var currentUser = HttpContext.User as Custom2IPrincipal;

            List = db.Database.SqlQuery<UnsubscribedResponse>("sp_SubscribedSendSurvey @UserName, @UserEmail, @SentByTeam, @SubscriptionID ",
                            new SqlParameter("UserName", currentUser.FullName),
                            new SqlParameter("UserEmail", currentUser.EmailAddress),
                            new SqlParameter("SentByTeam", currentUser.GroupID),
                            new SqlParameter("SubscriptionID", subscriptionID)).FirstOrDefault();

            return Json(new { data = List }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveSendSurvey(int surveyID)
        {
            UnsubscribedResponse List = new UnsubscribedResponse();

            var currentUser = HttpContext.User as Custom2IPrincipal;

            List = db.Database.SqlQuery<UnsubscribedResponse>("sp_RemoveSendSurvey @UserName, @UserEmail, @SentByTeam, @SurveyID ",
                            new SqlParameter("UserName", currentUser.FullName),
                            new SqlParameter("UserEmail", currentUser.EmailAddress),
                            new SqlParameter("SentByTeam", currentUser.GroupID),
                            new SqlParameter("SurveyID", surveyID)).FirstOrDefault();

            return Json(new { data = List }, JsonRequestBehavior.AllowGet);
        }


        public long executeCommandBySP(string _sqlString, ArrayList _sqlparameters, bool retrieveID)
        {
            SqlConnection sqlCon = new SqlConnection(db.Database.Connection.ConnectionString);

            sqlCon.Open();
            long result = 0;

            if (retrieveID == true)
            {
                SqlCommand sqlCom = new SqlCommand(_sqlString, sqlCon);
                sqlCom.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter parameter in _sqlparameters)
                    sqlCom.Parameters.Add(parameter);

                SqlParameter returnParameter = sqlCom.Parameters.Add("@result", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;
                sqlCom.ExecuteNonQuery();
                result = (int)sqlCom.Parameters["@result"].Value;
            }
            else
            {
                SqlCommand sqlCom = new SqlCommand(_sqlString, sqlCon);
                sqlCom.CommandType = CommandType.StoredProcedure;
                // add parameters
                foreach (SqlParameter parameter in _sqlparameters)
                    sqlCom.Parameters.Add(parameter);
                result = sqlCom.ExecuteNonQuery();
            }

            sqlCon.Close();

            return result;
        }

        [HttpPost]
        public JsonResult ListUnSubscribedCustomers()
        {
            //List<SubscribedCustomer> List = new List<SubscribedCustomer>();

            var currentUser = HttpContext.User as SubscribedCustomer;

            var _list = db.Database.SqlQuery<string>("sp_ListUnSubscribedCustomer").ToList();

            string[] custArr = _list.ToArray();

            return Json(new { data = custArr }, JsonRequestBehavior.AllowGet);
        }

        private List<SendSurveyAdmins> ListUserAdmins()
        {
            var _list = db.Database.SqlQuery<SendSurveyAdmins>("sp_ListCSATUsers").ToList();

            return _list;
        }

        //[HttpPost]
        //public JsonResult ListAdmins(string emailAddress)
        //{
        //    //List<SubscribedCustomer> List = new List<SubscribedCustomer>();
        //    var currentUser = HttpContext.User as SubscribedCustomer;

        //    var _list = emailAddress == "" ? ListUserAdmins().Where(i=>i.isAdmin == true) : ListUserAdmins().Where(i=>i.isAdmin == true && i.Email.Contains(emailAddress));

        //    return Json(new { data = _list }, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ListUsers(string emailAddress)
        //{
        //    //List<SubscribedCustomer> List = new List<SubscribedCustomer>();
        //    var currentUser = HttpContext.User as SubscribedCustomer;

        //    var _list = emailAddress == "" ? ListUserAdmins().Where(i => i.isAdmin == false) : ListUserAdmins().Where(i => i.isAdmin == false && i.Email.Contains(emailAddress));

        //    return Json(new { data = _list }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult GenerateReport(string dateStart, string dateEnd)
        {
            string fileName = string.Format("SendSurveyReport{0}.xls", DateTime.Now.ToString("MMddyy"));

            var localPath = Server.MapPath("~/content/TelerikTemplates/CSATSendSurveyReport.trdx");
            var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();

            System.Xml.XmlReaderSettings settings = new System.Xml.XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            Telerik.Reporting.Report reportDocument = new Telerik.Reporting.Report();

            using (System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(localPath, settings))
            {
                Telerik.Reporting.XmlSerialization.ReportXmlSerializer xmlSerializer =
                    new Telerik.Reporting.XmlSerialization.ReportXmlSerializer();

                reportDocument = (Telerik.Reporting.Report)
                    xmlSerializer.Deserialize(xmlReader);

                reportDocument.ReportParameters["startDate"].Value = dateStart;
                reportDocument.ReportParameters["endDate"].Value = dateEnd;

            }
            reportDocument.DocumentName = fileName;

            var reportResult = reportProcessor.RenderReport("xls", reportDocument, null);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string localPathForDownload = saveToReportFolder(reportResult, fileName);

            return Json(new { result = localPathForDownload, isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        string saveToReportFolder(Telerik.Reporting.Processing.RenderingResult report_, string fileName)
        {
            var localPath = Server.MapPath("~/Resources/Downloads/" + fileName);

            bool exists = System.IO.Directory.Exists(Server.MapPath("~/Resources/Downloads/"));

            if (!exists)
                System.IO.Directory.CreateDirectory(Server.MapPath("~/Resources/Downloads/"));

            if (System.IO.File.Exists(localPath))
            {
                System.IO.File.Delete(localPath);

                System.IO.File.WriteAllBytes(localPath, report_.DocumentBytes);
            }
            else
            {
                System.IO.File.WriteAllBytes(localPath, report_.DocumentBytes);

            }

            return System.Configuration.ConfigurationManager.AppSettings["localReportPath"] + fileName;
        }

        [HttpPost]
        public JsonResult GenerateSpreadSheet(string dateStart, string dateEnd)
        {
            string fileName = string.Format("SendSurveyReport{0}.xlsx", DateTime.Now.ToString("MMddyy"));

            SLDocument sldoc = new SLDocument();
            SpreadSheetStylerUtility.SpreadSheetStyler sStyler = new SpreadSheetStylerUtility.SpreadSheetStyler();

            var List = db.Database.SqlQuery<SendSurveyReport>("sp_ListSendSurvey_Report2 @startDate, @endDate ",
                           new SqlParameter("startDate", dateStart),
                           new SqlParameter("endDate", dateEnd));


            sldoc.RenameWorksheet(SLDocument.DefaultFirstSheetName, "Sent Survey");
            //Customer Email  Subject Line    Sent By Sent by Group Date Sent Date  Status

            sldoc.SetCellValue("A1", "Customer Email");
            sldoc.SetCellValue("B1", "Subject Line");
            sldoc.SetCellValue("C1", "Sent By");
            sldoc.SetCellValue("D1", "Sent By Group");
            sldoc.SetCellValue("E1", "Date Sent");
            sldoc.SetCellValue("F1", "Status");
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleFont(true, 9, "Tahoma", System.Drawing.Color.Black));
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleFill(System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.Color.White));

            int currentRow = 2;

            foreach (var reps in List)
            {
                sldoc.SetCellValue(currentRow, 1, reps.CustomerEmail);
                sldoc.SetCellValue(currentRow, 2, reps.SubjectLine);

                sldoc.SetCellValue(currentRow, 3, reps.SentBy);
                sldoc.SetCellValue(currentRow, 4, reps.SentByGroup);
                sldoc.SetCellValue(currentRow, 5, reps.DateSent.ToString("MM/dd/yyy hh:mm:ss tt"));
                sldoc.SetCellValue(currentRow, 6, reps.Status);

                sldoc.SetCellStyle(currentRow, 3, currentRow, 6, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 3, currentRow, 6, sStyler.styleFont(false, 9, "Tahoma", System.Drawing.Color.Black));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
            }


            sldoc.AutoFitColumn(1, 6);

            //sp_ListSendSurveyPerGroup_Report
            var listGroupCount = db.Database.SqlQuery<SendSurveyGroupReport>("sp_ListSendSurveyPerGroup_ReportV2 @startDate, @endDate ",
                      new SqlParameter("startDate", dateStart),
                      new SqlParameter("endDate", dateEnd));

            sldoc.SetCellValue("H1", "Group");

            sldoc.SetCellValue("I1", "Survey Survey");
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleFont(true, 9, "Tahoma", System.Drawing.Color.Black));
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleFill(System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.Color.White));

            currentRow = 2;

            foreach (var grp in listGroupCount)
            {
                sldoc.SetCellValue(currentRow, 8, grp.Group);
                sldoc.SetCellValue(currentRow, 9, grp.TotalCount);

                sldoc.SetCellStyle(currentRow, 9, currentRow, 9, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 8, currentRow, 9, sStyler.styleFont(false, 9, "Tahoma", System.Drawing.Color.Black));
                sldoc.SetCellStyle(currentRow, 8, currentRow, 9, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
            }
            sldoc.AutoFitColumn(8, 9);




            //Saved
            sldoc.AddWorksheet("Saved Survey");
            var listSavedSurvey = db.Database.SqlQuery<SendSurveyReport>("sp_ListSendSurvey_Report2_NotSent @startDate, @endDate ",
                  new SqlParameter("startDate", dateStart),
                  new SqlParameter("endDate", dateEnd));


            sldoc.SetCellValue("A1", "Customer Email");
            sldoc.SetCellValue("B1", "Subject Line");
            sldoc.SetCellValue("C1", "Sent By");
            sldoc.SetCellValue("D1", "Sent By Group");
            sldoc.SetCellValue("E1", "Date Added");
            sldoc.SetCellValue("F1", "Status");
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleFont(true, 9, "Tahoma", System.Drawing.Color.Black));
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.SetCellStyle(1, 1, 1, 6, sStyler.styleFill(System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.Color.White));

            currentRow = 2;

            foreach (var repsSvd in listSavedSurvey)
            {
                sldoc.SetCellValue(currentRow, 1, repsSvd.CustomerEmail);
                sldoc.SetCellValue(currentRow, 2, repsSvd.SubjectLine);

                sldoc.SetCellValue(currentRow, 3, repsSvd.SentBy);
                sldoc.SetCellValue(currentRow, 4, repsSvd.SentByGroup);
                sldoc.SetCellValue(currentRow, 5, repsSvd.DateSent.ToString("MM/dd/yyy hh:mm:ss tt"));
                sldoc.SetCellValue(currentRow, 6, repsSvd.Status);

                sldoc.SetCellStyle(currentRow, 3, currentRow, 6, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 3, currentRow, 6, sStyler.styleFont(false, 9, "Tahoma", System.Drawing.Color.Black));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
            }


            sldoc.AutoFitColumn(1, 6);

            //sp_ListSendSurveyPerGroup_Report
            var listGroupCountSaved = db.Database.SqlQuery<SendSurveyGroupReport>("sp_ListSendSurveyPerGroup_ReportV2_NotSent @startDate, @endDate ",
                      new SqlParameter("startDate", dateStart),
                      new SqlParameter("endDate", dateEnd));

            sldoc.SetCellValue("H1", "Group");


            sldoc.SetCellValue("I1", "Survey Sent");
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleFont(true, 9, "Tahoma", System.Drawing.Color.Black));
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.SetCellStyle(1, 8, 1, 9, sStyler.styleFill(System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.ColorTranslator.FromHtml("#000000"), System.Drawing.Color.White));

            currentRow = 2;

            foreach (var grpSvd in listGroupCountSaved)
            {
                sldoc.SetCellValue(currentRow, 8, grpSvd.Group);
                sldoc.SetCellValue(currentRow, 9, grpSvd.TotalCount);

                sldoc.SetCellStyle(currentRow, 9, currentRow, 9, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 8, currentRow, 9, sStyler.styleFont(false, 9, "Tahoma", System.Drawing.Color.Black));
                sldoc.SetCellStyle(currentRow, 8, currentRow, 9, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
            }
            sldoc.AutoFitColumn(8, 9);


            string strFileName = Server.MapPath("~/Resources/Downloads/" + fileName);

            sldoc.SaveAs(strFileName);

            string localPath = System.Configuration.ConfigurationManager.AppSettings["localReportPath"] + fileName;

            return Json(new { result = localPath, isSuccess = true }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GenerateSRSpreadSheet(int month, int year, int quarter, int paramType)
        {
            string fileName = string.Format("CSATServiceRecovery{0}.xlsx", DateTime.Now.ToString("MMddyy"));
            DateTime customDate = paramType == 1 ? new DateTime(year, month, 1) : DateTime.Now;
            var currentUser = HttpContext.User as Custom2IPrincipal;
            string isAdminServiceRecovery = ConfigurationManager.AppSettings["isAdminServiceRecovery"].ToString();
            var listOfAdminServiceRecovery = isAdminServiceRecovery.Split(',');

            SLDocument sldoc = new SLDocument();
            SpreadSheetStylerUtility.SpreadSheetStyler sStyler = new SpreadSheetStylerUtility.SpreadSheetStyler();
            List<ServiceRecoveryList> listServiceRecovery = new List<ServiceRecoveryList>();

          


            if (Array.Exists(listOfAdminServiceRecovery, E => E == currentUser.UserID.ToString())) {
                listServiceRecovery = db.Database.SqlQuery<ServiceRecoveryList>("sp_CSAT_ListServiceRecovery_Report @month, @year, @quarter, @paramType",
                                  new SqlParameter("month", month),
                                  new SqlParameter("year", year),
                                  new SqlParameter("quarter", quarter),
                                  new SqlParameter("paramType", paramType)).ToList();
            }
            else
            {
                listServiceRecovery = db.Database.SqlQuery<ServiceRecoveryList>("sp_CSAT_ListServiceRecovery_Report @month, @year, @quarter, @paramType",
                                  new SqlParameter("month", month),
                                  new SqlParameter("year", year),
                                  new SqlParameter("quarter", quarter),
                                  new SqlParameter("paramType", paramType)).Where(i=>i.AddedByGroupID == currentUser.GroupID).ToList();
            }

            sldoc.RenameWorksheet(SLDocument.DefaultFirstSheetName, "Service Recovery");
            //Customer Email  Subject Line    Sent By Sent by Group Date Sent Date  Status
            string quarterString = quarter == 1 ? "1st Quarter" : quarter == 2 ? "2nd Quarter" : quarter == 3 ? "3rd Quarter" : "4th Quarter";

            string header = paramType == 1 ? string.Format("CSAT Service Recovery - {0}", customDate.ToString("MMM - yyyy"))
                     : paramType == 2 ? string.Format("CSAT Service Recovery - {0} {1}", quarterString, year)
                     : string.Format("CSAT Service Recovery - Fiscal {0}", year);

            sldoc.SetCellValue("A1", header);
            sldoc.SetCellStyle(1, 1, 1, 1, sStyler.styleFont(true, 12, "Tahoma", System.Drawing.Color.Black));
            sldoc.MergeWorksheetCells("A1", "M1");
            //sldoc.SetCellStyle(1, 1, 1, 1, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));

            sldoc.SetCellValue("A3", "Date Received");
            sldoc.SetCellValue("B3", "Customer");
            sldoc.SetCellValue("C3", "Contact Name");
            sldoc.SetCellValue("D3", "Detractor");
            sldoc.SetCellValue("E3", "Issue Raised");
            sldoc.AutoFitColumn(1, 5);

            sldoc.SetCellValue("F3", "Approved Callback/Service Recovery?");
            sldoc.SetColumnWidth("F3", 15.00);
            sldoc.SetCellValue("G3", "Service Recovery Attempt Made?");
            sldoc.SetColumnWidth("G3", 15.00);
            sldoc.SetCellValue("H3", "Date of Contact");
            sldoc.SetCellValue("I3", "Contacted By");
            sldoc.AutoFitColumn(8, 9);

            sldoc.SetCellValue("J3", "Successful Attempt to Contact?");
            sldoc.SetColumnWidth("J3", 15.00);

            sldoc.SetCellValue("K3", "Is there effort to address issue raised?");
            sldoc.SetColumnWidth("K3", 30.00);

            sldoc.SetCellValue("L3", "Resolution");
            sldoc.SetColumnWidth("L3", 20.00);

            sldoc.SetCellValue("M3", "Remarks");
            sldoc.SetColumnWidth("M3", 20.00);

            sldoc.SetCellValue("N3", "Service Recovery Attempt within target? - within 48hrs upon receipt of Detractor Notification");
            sldoc.SetColumnWidth("N3", 20.00);

            sldoc.SetCellValue("O3", "Added By");
            sldoc.SetCellValue("P3", "Group");
            sldoc.SetCellValue("Q3", "Date Added");

            sldoc.AutoFitColumn(15, 17);


            sldoc.SetCellStyle(3, 1, 3, 17, sStyler.styleFont(true, 9, "Tahoma", System.Drawing.Color.Black));
            sldoc.SetCellStyle(3, 1, 3, 17, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.SetCellStyle(3, 1, 3, 17, sStyler.styleFill(System.Drawing.ColorTranslator.FromHtml("#FFFF00"), System.Drawing.ColorTranslator.FromHtml("#FFFF00"), System.Drawing.Color.Black));
                                         
            sldoc.SetCellStyle(3, 1, 3, 17, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetRowHeight(3, 70.00);

            int currentRow = 4;

            foreach (var sr in listServiceRecovery)
            {
                sldoc.SetCellValue(currentRow, 1, sr.DateReceived);
                sldoc.SetCellValue(currentRow, 2, sr.Customer);
                sldoc.SetCellValue(currentRow, 3, sr.ContactName);
                sldoc.SetCellValue(currentRow, 4, sr.Detractor);
                sldoc.SetCellValue(currentRow, 5, sr.IssueRaised);
                sldoc.SetCellValue(currentRow, 6, sr.IsApprovedCallBackService);
                sldoc.SetCellValue(currentRow, 7, sr.ServiceRecoveryAttemptMade);
                sldoc.SetCellValue(currentRow, 8, sr.DateOfContact);
                sldoc.SetCellValue(currentRow, 9, sr.ContactedBy);
                sldoc.SetCellValue(currentRow, 10, sr.SuccessfulAttemptContact);
                sldoc.SetCellValue(currentRow, 11, sr.IsThereEffort);
                sldoc.SetCellValue(currentRow, 12, sr.Resolution);
                sldoc.SetCellValue(currentRow, 13, sr.Remarks);
                sldoc.SetCellValue(currentRow, 14, sr.ServiceRecoveryWithinTarget);
                sldoc.SetCellValue(currentRow, 15, sr.FullName);
                sldoc.SetCellValue(currentRow, 16, sr.AddedByGroup);
                sldoc.SetCellValue(currentRow, 17, sr.DateAdded);

                sldoc.AutoFitColumn(15, 17);

                sldoc.SetCellStyle(currentRow, 1, currentRow, 17, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 17, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));

                currentRow++;
            }

            int lastRowCount = currentRow - 1;

            currentRow = currentRow + 2;
            int detractorRowCount = currentRow;

            sldoc.SetCellValue(currentRow, 1, "Detractor CSAT");
            sldoc.SetCellValue(currentRow, 2, "=COUNTIF(D4:D" + lastRowCount.ToString() + ",\"CSAT\")");
            sldoc.SetCellStyle(currentRow, 2, currentRow, 2, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

            sldoc.SetCellValue(currentRow, 5, "Total Service Recovery Attempt within Target");
            sldoc.SetCellStyle(currentRow, 5, currentRow, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.MergeWorksheetCells(currentRow, 5, currentRow, 6);

            currentRow++;
            sldoc.SetCellValue(currentRow, 1, "Detractor CES");
            sldoc.SetCellValue(currentRow, 2, "=COUNTIF(D4:D" + lastRowCount.ToString() + ",\"CES\")");
            sldoc.SetCellStyle(currentRow, 2, currentRow, 2, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

            sldoc.SetCellValue(currentRow, 5, "YES");
            sldoc.SetCellValue(currentRow, 6, "=COUNTIF(N4:N" + lastRowCount.ToString() + ",\"YES\")");
            sldoc.SetCellStyle(currentRow, 6, currentRow, 6, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 5, currentRow, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

            currentRow++;
            sldoc.SetCellValue(currentRow, 1, "Detractor CSAT and CES");
            sldoc.SetCellValue(currentRow, 2, "=COUNTIF(D4:D" + lastRowCount.ToString() + ",\"BOTH\")");
            sldoc.SetCellStyle(currentRow, 2, currentRow, 2, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

            sldoc.SetCellValue(currentRow, 5, "NO");
            sldoc.SetCellValue(currentRow, 6, "=COUNTIF(N4:N" + lastRowCount.ToString() + ",\"NO\")");
            sldoc.SetCellStyle(currentRow, 6, currentRow, 6, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 5, currentRow, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

            currentRow++;
            sldoc.SetCellValue(currentRow, 1, "Total Detractor");
            sldoc.SetCellValue(currentRow, 2, "=SUM(B" + detractorRowCount.ToString() + ":B" + (currentRow - 1).ToString() + ")");
            sldoc.SetCellStyle(currentRow, 2, currentRow, 2, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

            int totalDetractor = currentRow;

            currentRow = currentRow + 2;

            sldoc.SetCellValue(currentRow, 1, "% Service Recovery Effort - no. of entries that meet Nos. 1 and 2 below, divided by the total number of entries for the given period");
            sldoc.MergeWorksheetCells(currentRow, 1, currentRow, 20);
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            sldoc.AutoFitRow(currentRow, 1);

            currentRow++;
            sldoc.SetCellValue(currentRow, 1, "1) Col F = Yes, Col G = Yes");
            sldoc.SetCellValue(currentRow, 2, "=(COUNTIFS(F4:F" + lastRowCount.ToString() + ",\"YES\", G4:G" + lastRowCount.ToString() + ",\"YES\"))/B" + totalDetractor.ToString() + "");
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleText(true, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            int criteria1 = currentRow;
            //sldoc.AutoFitColumn(currentRow, 1);

            currentRow++;
            sldoc.SetCellValue(currentRow, 1, "2) Col F = No, Col K = Yes");
            sldoc.SetCellValue(currentRow, 2, "=(COUNTIFS(F4:F" + lastRowCount.ToString() + ",\"NO\", K4:K" + lastRowCount.ToString() + ",\"YES\"))/B" + totalDetractor.ToString() + "");
            sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, "0.00%"));
            //sldoc.AutoFitColumn(currentRow, 1);
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            int criteria2 = currentRow;

            currentRow++;
            sldoc.SetCellValue(currentRow, 1, "Total Service Recovery %");
            sldoc.SetCellValue(currentRow, 2, "=SUM(" + SLConvert.ToCellReference(criteria1, 2) + ":" + SLConvert.ToCellReference(criteria2, 2) + ")");
            sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, "0.00%"));
            //sldoc.AutoFitColumn(currentRow, 1);
            sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));


            if (Array.Exists(listOfAdminServiceRecovery, E => E == currentUser.UserID.ToString()))
            {
                currentRow = currentRow + 2;
                sldoc.SetCellValue(currentRow, 1, "Count per Group");
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
                sldoc.AutoFitRow(currentRow, 1);

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CE - Inside Sales");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CE - Inside Sales\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CE Survey- Customer Service");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CE Survey- Customer Service\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CE Survey - Managed Services");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CE Survey - Managed Services\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CE Survey- Application Engineers");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CE Survey- Application Engineers\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CE Survey Technical Services V2");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CE Survey Technical Services V2\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CES Survey - C+ Customer Success");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CES Survey - C+ Customer Success\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CES Survey - Data Analytics");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CES Survey - Data Analytics\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));

                currentRow++;
                sldoc.SetCellValue(currentRow, 1, "CSAT/CE Survey - Field Project Services");
                sldoc.SetCellValue(currentRow, 2, "=COUNTIF(P4:P" + lastRowCount.ToString() + ",\"CSAT/CE Survey - Field Project Services\")");
                sldoc.SetCellStyle(SLConvert.ToCellReference(criteria1, 2), SLConvert.ToCellReference(currentRow, 2), sStyler.styleText(false, VerticalAlignmentValues.Center, HorizontalAlignmentValues.Center, ""));
                sldoc.SetCellStyle(currentRow, 1, currentRow, 2, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
            }

            sldoc.AutoFitColumn(1);

            string strFileName = Server.MapPath("~/Resources/Downloads/" + fileName);

            sldoc.SaveAs(strFileName);

            string localPath = System.Configuration.ConfigurationManager.AppSettings["localReportPath"] + fileName;

            return Json(new { result = localPath, isSuccess = true }, JsonRequestBehavior.AllowGet);
        }

    }
}

