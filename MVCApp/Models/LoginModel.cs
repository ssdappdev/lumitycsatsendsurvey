﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace MVCApp.Models
{
    public class Login
    {
        public String Username { get; set; }
        public String Password { get; set; }
        public int Group { get; set; }
        public bool RememberMe { get; set; }
        public bool IsSuccess { get; set; }
        public int RoleID { get; set; }

        public String ErrorMessage { get; set; }
    }


    public class CustomPrincipalSerializeModel
    {
        public string Username { get; set; }
        public int EmpID { get; set; }
        public int RoleID { get; set; }
        public string EmailAddress { get; set; }
        public string FullName { get; set; }
        public int GroupID { get; set; }
        public bool isAdmin { get; set; }
        public string Avatar { get; set; }
        public string Title { get; set; }
    }
    interface CustomIPrincipal : IPrincipal
    {
        int UserID { get; set; }
        string Username { get; set; }
        int RoleID { get; set; }
        string EmailAddress { get; set; }
        string FullName { get; set; }
        bool isAdmin { get; set; }
        string UserName { get; set; }
        int GroupID { get; set; } // 1 = Inside Sales, 2 = Customer Services, 3 = Lumity, 4 = Technical Services
    }

    public class Custom2IPrincipal : CustomIPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role) { return false; }

        public Custom2IPrincipal(string Name)
        {
            this.Identity = new GenericIdentity(Name);
        }


        public string Username { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string EmailAddress { get; set; }
        public string FullName { get; set; }
        public bool isAdmin { get; set; }
        public string UserName { get; set; }
        public int GroupID { get; set; } // 1 = Inside Sales, 2 = Customer Services, 3 = Lumity, 4 = Technical Services
        public string Avatar { get; set; }
        public string Title { get; set; }
    }
}