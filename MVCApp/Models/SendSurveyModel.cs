﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCApp.Models
{
    public class ServiceRecoveryModel
    {
        public int ID { get; set; }

        public DateTime DateReceived { get; set; }
        public string Customer { get; set; }
        public string ContactName { get; set; }
        public string Detractor { get; set; }
        public string IssueRaised { get; set; }
        public bool ServiceRecoveryAttemptMade { get; set; }
        public DateTime DateOfContact { get; set; }
        public string ContactedBy { get; set; }
        public bool SuccessfulAttemptToContact { get; set; }
        public bool IsThereEffort { get; set; }
        public int ResolutionID { get; set; }
        public string Remarks { get; set; }
        public bool ServiceRecoveryWithinTarget { get; set; }
        public int AddedByID { get; set; }
        public int? UpdatedByID { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateUpdated { get; set; }
        public bool IsApprovedCallBackService { get; set; }
        public bool isActive { get; set; }
        public int? RemovedByID { get; set; }
        public DateTime? DateRemoved { get; set; }
    }

    public class ServiceRecoveryList
    {
        public int ID { get; set; }
        public string DateReceived { get; set; }
        public string Customer { get; set; }
        public string ContactName { get; set; }
        public string Detractor { get; set; }
        public string IssueRaised { get; set; }
        public string IsApprovedCallBackService { get; set; }
        public string ServiceRecoveryAttemptMade { get; set; }
        public string DateOfContact { get; set; }
        public string ContactedBy { get; set; }
        public string SuccessfulAttemptContact { get; set; }
        public string IsThereEffort { get; set; }
        public string Remarks { get; set; }
        public string ServiceRecoveryWithinTarget { get; set; }
        public string AddedByGroup { get; set; }
        public int AddedByGroupID { get; set; }

        public string FullName { get; set; }
        public string DateAdded { get; set; }
        public string Resolution { get; set; }

    }
    public class ServiceRecovery
    {
        public int ID { get; set; }
        public DateTime dateReceived { get; set; }
        public string customer { get; set; }
        public string contactName { get; set; }
        public string detractor { get; set; }
        public string issueRaised { get; set; }
        public bool isApprovedCallBackService { get; set; }
        public bool isServiceRecoveryAttemptMade { get; set; }
        public string DateOfContact { get; set; }
        public string contactedBy { get; set; }
        public bool isSuccessfulAttemptContact { get; set; }
        public bool isThereEffort { get; set; }
        public string Remarks { get; set; }
        public bool isServiceRecoveryWithinTarget { get; set; }
        public int ResolutionID { get; set; }
    }

    public class SendSurveyAdmins
    {
        public int UserID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string RegisteredBy { get; set; }
        public DateTime DateRegistered { get; set; }
        public string RemovedBy { get; set; }
        public DateTime? DateRemoved { get; set; }
        public string Status { get; set; }
        public bool isAdmin { get; set; }
        public string UserName { get; set; }
        public int GroupID { get; set; } // 1 = Inside Sales, 2 = Customer Services, 3 = Lumity, 4 = Technical Services
    }

    public class EmailFromOutlook
    {
        public string SenderEmailAddress { get; set; }
        public string ToEmailAddress { get; set; }
        public string SubjectEmail { get; set; }
        public string Body { get; set; }
    }
    public class SendSurvey
    {
        public string CustomerEmail { get; set; }
        public bool isSubcribed { get; set; }
        public string SubjectLine { get; set; }

    }

    public class SubscribedCustomer
    {
        public string CustomerEmail { get; set; }
    }
    public class SendSurveyViewModel
    {
        public int SurveyID { get; set; }
        public string CustomerEmail { get; set; }
        public string isSubscribed { get; set; }
        public string SubjectLine { get; set; }
        public string SentBy { get; set; }
        public string Status { get; set; }
        public string SentByGroup { get; set; }
        public DateTime DateAdded { get; set; }
        public string DateSent { get; set; }
    }

    public class UnsubscribedCustomer
    {
        public int CustomerSurveyID { get; set; }
        public DateTime DateUnsubscribed { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string UnsubscribedByUserName { get; set; }
        public string UnsubscribedByEmail { get; set; }
    }

    public class UnsubscribedCustomerViewModel
    {
        public int CustomerSurveyID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string SentByGroup { get; set; }
        public DateTime DateUnsubscribed { get; set; }
        public string UnsubscribedByUserName { get; set; }
        public string UnsubscribedByEmail { get; set; }
    }

    public class SendSurveyResponse
    {
        public int isSuccess { get; set; }
        public string Message { get; set; }
    }


    public class UnsubscribedResponse
    {
        public int isSuccess { get; set; }
        public string Message { get; set; }
    }

    public class SendSurveyReport
    {
        public string CustomerEmail { get; set; }
        public DateTime DateSent { get; set; }
        public string SentBy { get; set; }
        public string SentByGroup { get; set; }
        public string Status { get; set; }
        public string SubjectLine { get; set; }
    }

    public class SendSurveyGroupReport
    {
        public string Group { get; set; }
        public int TotalCount { get; set; }
    }
}