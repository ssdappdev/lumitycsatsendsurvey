﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Net;


namespace MVCApp.Controllers
{

    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {

        public string currentController = String.Empty;
        public string currentAction = String.Empty;
        public string currentId = String.Empty;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            currentController = (String)filterContext.RouteData.Values["controller"];
            currentAction = (String)filterContext.RouteData.Values["action"];
            currentId = (String)filterContext.RouteData.Values["id"];
            base.OnAuthorization(filterContext);


          

        }
    }

    public class LoggedOrAuthorizedAttribute : AuthorizeAttribute
    {

        public LoggedOrAuthorizedAttribute()
        {
            View = "_SessionExpired";
            Master = String.Empty;
        }

        public String View { get; set; }
        public String Master { get; set; }
        public int StatusCode { get; set; }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            CheckIfUserIsAuthenticated(filterContext);
        }

        private void CheckIfUserIsAuthenticated(AuthorizationContext filterContext)
        {
            // If Result is null, we're OK: the user is authenticated and authorized. 
            if (filterContext.Result == null)
            {
                return;
            }
            else
            {
                //if (filterContext.Result.StatusCode ==  401)
                // If here, you're getting an HTTP 401 status code. In particular,
                // filterContext.Result is of HttpUnauthorizedResult type. Check Ajax here. 
                // if (filterContext.HttpContext.User.Identity.IsAuthenticated) 
                //{
                // return View("SessionExpired", "Login");
                //if (String.IsNullOrEmpty(View)) 
                //   return; 
                var result = new ViewResult { ViewName = View, MasterName = Master };
                filterContext.Result = result;
            }
        }
    }
}