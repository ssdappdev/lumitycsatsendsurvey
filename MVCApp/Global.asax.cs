﻿using MVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using System.Web.Script.Serialization;
using System.Web.Security;


namespace MVCApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);

                Custom2IPrincipal newUser = new Custom2IPrincipal(authTicket.Name);
                newUser.UserID = serializeModel.EmpID;
                newUser.Username = serializeModel.Username;
                newUser.RoleID = serializeModel.RoleID;
                newUser.EmailAddress = serializeModel.EmailAddress;
                newUser.FullName = serializeModel.FullName;
                newUser.GroupID = serializeModel.GroupID;
                newUser.Avatar = serializeModel.Avatar;
                newUser.Title = serializeModel.Title;

                HttpContext.Current.User = newUser;
            }
        }
    }
}
