﻿
var $ = jQuery.noConflict();
var MAIN = MAIN || {};
(function ($) {

    //*--- AJAX Request
    MAIN.data = {
        init: function () {
            // MAIN.data.setURL();
            //MAIN.data.selectizeExtendedPlugin();
        },
        selectizeExtendedPlugin: function(){
            Selectize.define('select_remove_all_options', function (options) {
                if (this.settings.mode === 'single') return;

                var self = this;

                self.setup = (function () {
                    var original = self.setup;
                    return function () {
                        original.apply(this, arguments);

                        var allBtn = $('<button type="button" class="btn btn-xs btn-success">Select All</button>');
                        var clearBtn = $('<button type="button" class="btn btn-xs btn-default">Clear</button>');
                        var btnGrp = $('<div class="selectize-plugin-select_remove_all_options-btn-grp"></div>');
                        btnGrp.append(allBtn, ' ', clearBtn);

                        allBtn.on('click', function () {
                            self.setValue($.map(self.options, function (v, k) {
                                return k
                            }));
                        });
                        clearBtn.on('click', function () {
                            self.setValue([]);
                        });

                        this.$wrapper.append(btnGrp)
                    };
                })();
            });
        },
        employeeListMonTechOnly: function (SelectElement, Selectize) {
            jQuery.ajax({
                type: "post"
                , url: appURL + "/Accounts/GetAccountsMonTech"
                , data: ""
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: ""
                , success: function (data) {
                    $('#preLoader').hide();
                    var html = "";
                    $.each(data.data, function (i, item) {
                        html += "<option value='" + item.EmpID + "'>" + item.Firstname + " " + item.Lastname + "</option>";
                    });
                    $(SelectElement).append(html);
                    if (Selectize) {

                        $(SelectElement).selectize({
                            placeholder: "-- Select or type in Employe Name --",
                            plugins: ['remove_button']
                        });
                    }
                }
            });
        },
        employeeList: function(SelectElement,Selectize) {
            jQuery.ajax({
                type: "post"
                , url: appURL + "/Accounts/GetAccounts"
                , data: ""
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: ""
                , success: function (data) {
                    $('#preLoader').hide();
                    var html = "";
                    $.each(data.data, function (i, item) {
                        html += "<option value='" + item.EmpID + "'>" + item.Firstname + " " + item.Lastname + "</option>";
                    });
                    $(SelectElement).append(html);
                    if (Selectize) {

                        $(SelectElement).selectize({
                            placeholder: "-- Select or type in Employe Name --",
                            plugins: ['remove_button']
                        });
                    }
                }
            });
        },
    },

    
    /*--- DOCUMENT ON READY ---*/
    MAIN.documentOnReady = {
        init: function () {
            MAIN.data.init();
        }
    };




    //*--- END Function
})(jQuery);

//* Instantiate all functions declared on MAIN.documentOnReady.init
$(document).ready(MAIN.documentOnReady.init);

