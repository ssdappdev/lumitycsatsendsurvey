﻿
using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Net;

public class MailMaster
{
    private string host = ConfigurationManager.AppSettings["SMTPServerIP"].ToString();
    private string isTest = ConfigurationManager.AppSettings["isTestEmail"].ToString();

    public MailMaster() { }

    public void sendSurveyEmail(string _customerEmail, string _message, string _subject, string _headerPath1, string _headerPath2)
    {
        try
        {
            string body = _message;
            SmtpClient client = new SmtpClient();
            client.Host = host;
            client.Port = 25;
            MailMessage message = new MailMessage();

            message.From = new MailAddress("ColdChain.CSATsurvey@emerson.com", "CSATsurvey, ColdChain");
            message.To.Add(new MailAddress(_customerEmail));

            //var _custEmails = _customerEmail.Split(';');
            //foreach (var ce in _custEmails)
            //{
            //    message.To.Add(new MailAddress(ce));
            //}

            message.Subject = string.Format("Copeland Cold Chain CSAT / CE Survey:  {0} ",_subject);
            message.IsBodyHtml = true;
            message.Body = body;

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            LinkedResource theEmailImage1 = new LinkedResource(_headerPath1, "image/jpeg");
            theEmailImage1.ContentId = "Picture1";
            theEmailImage1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
            htmlView.LinkedResources.Add(theEmailImage1);

            LinkedResource theEmailImage2 = new LinkedResource(_headerPath2, "image/jpeg");
            theEmailImage2.ContentId = "Picture2";
            theEmailImage2.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
            htmlView.LinkedResources.Add(theEmailImage2);

            message.AlternateViews.Add(htmlView);

            if (isTest == "0")
            {
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.Send(message);
                client.Dispose();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error sending email, please screen shot this and send to IT Support : " + ex.Message);
        }
    }
}