﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SpreadsheetLight;
using DocumentFormat.OpenXml.Spreadsheet;

/// <summary>
/// Summary description for SpreadSheetStyler
/// </summary>
/// 
namespace SpreadSheetStylerUtility
{
    public class SpreadSheetStyler
    {
        public SpreadSheetStyler()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public void insertPicture(SLDocument sl, string pathName, int top, int left)
        {
            SpreadsheetLight.Drawing.SLPicture pic = new SpreadsheetLight.Drawing.SLPicture(pathName);
            pic.SetPosition(top, left);
            sl.InsertPicture(pic);
        }

        public SLStyle styleBorder(BorderStyleValues top, BorderStyleValues bottom, BorderStyleValues left, BorderStyleValues right)
        {
            SLStyle style = new SLStyle();
            style.Border.TopBorder.BorderStyle = top;
            style.Border.BottomBorder.BorderStyle = bottom;
            style.Border.LeftBorder.BorderStyle = left;
            style.Border.RightBorder.BorderStyle = right;
            return style;
        }

        public SLStyle styleText(bool iswrap, VerticalAlignmentValues vertical, HorizontalAlignmentValues horizontal, string formatcode)
        {
            SLStyle style = new SLStyle();
            style.Alignment.WrapText = iswrap;
            style.Alignment.Vertical = vertical;
            style.Alignment.Horizontal = horizontal;
            style.FormatCode = formatcode;
            return style;
        }

        public SLStyle styleFont(bool isbold, int fontsize, string fontname, System.Drawing.Color fontcolor)
        {
            SLStyle style = new SLStyle();
            style.Font.Bold = isbold;
            style.Font.FontSize = fontsize;
            style.Font.FontName = fontname;
            style.Font.FontColor = fontcolor;
            return style;
        }

        public SLStyle styleFill(System.Drawing.Color foreground, System.Drawing.Color background, System.Drawing.Color fontcolor)
        {
            SLStyle style = new SLStyle();
            style.SetPatternFill(PatternValues.Solid, foreground, background);
            style.Font.FontColor = fontcolor;
            return style;
        }

    }
}