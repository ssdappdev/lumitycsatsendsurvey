﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCApp.Helpers
{
    public class DirectorySearcher
    {
        public Tuple<string, string> GetADInformation(string emailAddress)
        {

            string _emailAddress = emailAddress;
            string _userName = "";
            string emailDisplayName = "";
            string path = "LDAP://DC=emrsn,DC=org";
            string filterAttributes = "";

            string _adminUsername = "PmAmrDcs-SvcRmtWeb";
            string _adminPassword = "UX1OFMQI[^S9EE\\";
            string _adminPath = "LDAP://DC=emrsn,DC=org";

            System.DirectoryServices.DirectoryEntry de = new System.DirectoryServices.DirectoryEntry(path, _adminUsername, _adminPassword);

            try
            {
                Object obj = de.NativeObject;
                System.DirectoryServices.DirectorySearcher ds = new System.DirectoryServices.DirectorySearcher(de);
                //ds.Filter = "(SAMAccountName=" + _username + ")";
                ds.Filter = "(mail=" + _emailAddress + ")";

                ds.PropertiesToLoad.Add("SAMAccountName");
                ds.PropertiesToLoad.Add("cn");
                ds.PropertiesToLoad.Add("mail");
                ds.PropertiesToLoad.Add("company");
                ds.PropertiesToLoad.Add("co");
                ds.PropertiesToLoad.Add("givenName");
                ds.PropertiesToLoad.Add("SN");

                System.DirectoryServices.SearchResult result = ds.FindOne();

                if (result != null)
                {
                    path = result.Path;

                    filterAttributes = (String)result.Properties["SAMAccountName"][0];
                    emailDisplayName = (String)result.Properties["givenName"][0] + " " + (String)result.Properties["SN"][0];

                    _userName = filterAttributes;

                    var tup = new Tuple<string, string>(emailDisplayName, _userName);

                    return tup;
                }
                else
                {
                    _userName = "";
                    emailDisplayName = "";

                    var tup = new Tuple<string, string>(emailDisplayName, _userName);

                    return tup;
                }

            }
            catch (Exception ex)
            {
                _userName = "";
                emailDisplayName = "";

                var tup = new Tuple<string, string>(emailDisplayName, _userName);

                return tup;
            }
        }
    }
}