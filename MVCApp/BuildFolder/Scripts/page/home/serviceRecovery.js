﻿

//if approvedServiceRecovery Yes 
//serviceRecoveryAttemptMade = Yes

//if approvedServiceRecovery No
//Is isThereEffortIssueRaised = Yes

function disabledFields() {
    //$('#dateContact').prop('disabled', true);
    //$('#successAttemptContact').prop('disabled', true);
    $('#isApprovedServiceRecoveryYes').hide();
    $('#isServiceRecoveryAttemptMadeYes').hide();
    $('#isServiceRecoveryWithinTarget').hide();
}

function validateAttemptMade() {
    var isApprovedServiceRecovery = document.getElementById('approvedServiceRecovery').checked;

    if (isApprovedServiceRecovery) {
        $('#serviceRecoveryAttemptMade').prop('disabled', false);
        $('#isApprovedServiceRecoveryYes').show();

    }
    else {
        //$('#isThereEffortIssueRaised').attr('checked', true);
        $('#serviceRecoveryAttemptMade').prop('checked', false);
        $('#successAttemptContact').prop('checked', false);

        $('#dateContact').val("");

        $('#serviceRecoveryAttemptMade').prop('disabled', true);
        $('#successAttemptContact').prop('disabled', true);
        $('#isApprovedServiceRecoveryYes').hide();
        $('#isServiceRecoveryAttemptMadeYes').hide();

    }
}

$('#serviceRecoveryAttemptMade').change(function () {
    validateServiceRecoveryMade();
});


function validateServiceRecoveryMade()
{

    var isServiceRecoveryAttemptMade = document.getElementById('serviceRecoveryAttemptMade').checked;

    if (isServiceRecoveryAttemptMade) {
        $('#dateContact').prop('disabled', false);
        $('#successAttemptContact').prop('disabled', false);
        $('#isServiceRecoveryAttemptMadeYes').show();

        $('#isServiceRecoveryWithinTarget').show();
    }
    else {
        //$('#isThereEffortIssueRaised').attr('checked', true);
        $('#dateContact').prop('disabled', true);
        $('#successAttemptContact').prop('disabled', true);
        $('#isServiceRecoveryAttemptMadeYes').hide();

        $('#isServiceRecoveryWithinTarget').hide();
    }
}

function saveServiceRecovery() {

    var message = "";
    var isApprovedServiceRecovery = document.getElementById('approvedServiceRecovery').checked;
    var isServiceRecoveryAttemptMade = document.getElementById('serviceRecoveryAttemptMade').checked;

    if ($('#dateReceived').val() === "") {
        message += "Date Received is Required \n";
        $('#dateReceived').css('border', '1px solid red')
    }
    if (document.getElementById("detractor").options[document.getElementById("detractor").selectedIndex].value === "0") {
        message += "Detractor is Required \n";
        $('#detractor').css('border', '1px solid red')
    }
    if ($('#dateReceived').val() === "") {
        message += "Issue Raised is Required \n";
        $('#dateReceived').css('border', '1px solid red')
    }
    if (isApprovedServiceRecovery)
        if (isServiceRecoveryAttemptMade)
            if ($('#dateContact').val() === "") {
                message += "Date of Contact is Required \n";
                $('#dateContact').css('border', '1px solid red')
            }

    if (message.length > 0)
        alert(message);
    else {
        var obj = new Object();
  
        obj.dateReceived = $('#dateReceived').val();
        obj.customer = $('#customer').val();
        obj.contactName = $('#contactName').val();
        obj.detractor = document.getElementById("detractor").options[document.getElementById("detractor").selectedIndex].value;
        obj.issueRaised = $('#issueRaised').val();
        obj.isApprovedCallBackService = document.getElementById('approvedServiceRecovery').checked;
        obj.isServiceRecoveryAttemptMade = document.getElementById('serviceRecoveryAttemptMade').checked;

        obj.DateOfContact = document.getElementById('serviceRecoveryAttemptMade').checked ? $('#dateContact').val() : null;

        obj.contactedBy = $('#contactedBy').val();
        obj.isSuccessfulAttemptContact = document.getElementById('successAttemptContact').checked;
        obj.isThereEffort = document.getElementById('isThereEffortIssueRaised').checked;
        obj.Remarks = $('#remarks').val();
        obj.isServiceRecoveryWithinTarget = document.getElementById('serviceRecoveryAttemptMade').checked ? document.getElementById('serviceRecoveryWithinTarget').checked : false;
        obj.ResolutionID = document.getElementById("resolution").options[document.getElementById("resolution").selectedIndex].value;

        console.log(JSON.stringify(obj));

        jQuery.ajax({
            type: "post"
                      , url: urlAddServiceRecovery
                      , data: JSON.stringify(obj)
                      , contentType: "application/json; charset=UTF-8"
                      , beforeSend: function () { $('#preLoader').show(); }
                      , error: function () {
                          $('#preLoader').hide();
                      }
                      , success: function (response) {
                          loadUnsubscribeCustomer();
                          getListOfUnSubscribedCustomer();
                          loadServiceRecovery();

                          $('#preLoader').hide();
                          if (response.data.isSuccess == 0) {
                              MAIN.plugins.alertBox(0, response.data.Message);

                          } else {
                              MAIN.plugins.alertBox(1, response.data.Message);
                          }
                      }
        });
    }
}

function deleteServiceRecovery(id) {

    jQuery.ajax({
        type: "post"
                , url: urlDeleteServiceRecovery
                , data: JSON.stringify({ 'id': id })
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: ''
                , success: function (response) {
                    loadServiceRecovery();

                    $('#preLoader').hide();

                    if (response.data.isSuccess == 0) {
                        MAIN.plugins.alertBox(0, response.data.Message);

                    } else {
                        MAIN.plugins.alertBox(1, response.data.Message);
                    }
                }
    });
}

function updateServiceRecovery(id) {

    var message = "";
    var isApprovedServiceRecovery = document.getElementById('approvedServiceRecovery').checked;
    var isServiceRecoveryAttemptMade = document.getElementById('serviceRecoveryAttemptMade').checked;

    if ($('#dateReceived').val() === "") {
        message += "Date Received is Required \n";
        $('#dateReceived').css('border', '1px solid red')
    }
    if (document.getElementById("detractor").options[document.getElementById("detractor").selectedIndex].value === "0") {
        message += "Detractor is Required \n";
        $('#detractor').css('border', '1px solid red')
    }
    if ($('#dateReceived').val() === "") {
        message += "Issue Raised is Required \n";
        $('#dateReceived').css('border', '1px solid red')
    }
    if (isApprovedServiceRecovery)
        if (isServiceRecoveryAttemptMade)
            if ($('#dateContact').val() === "") {
                message += "Date of Contact is Required \n";
                $('#dateContact').css('border', '1px solid red')
            }

    if (message.length > 0)
        alert(message);
    else {
        debugger;
        var obj = new Object();
        obj.ID = id;
        obj.dateReceived = $('#dateReceived').val();
        obj.customer = $('#customer').val();
        obj.contactName = $('#contactName').val();
        obj.detractor = document.getElementById("detractor").options[document.getElementById("detractor").selectedIndex].value;
        obj.issueRaised = $('#issueRaised').val();
        obj.isApprovedCallBackService = document.getElementById('approvedServiceRecovery').checked;
        obj.isServiceRecoveryAttemptMade = document.getElementById('serviceRecoveryAttemptMade').checked;

        obj.DateOfContact = document.getElementById('serviceRecoveryAttemptMade').checked ? $('#dateContact').val() : null;

        obj.contactedBy = $('#contactedBy').val();
        obj.isSuccessfulAttemptContact = document.getElementById('successAttemptContact').checked;
        obj.isThereEffort = document.getElementById('isThereEffortIssueRaised').checked;
        obj.Remarks = $('#remarks').val();
        obj.isServiceRecoveryWithinTarget = document.getElementById('serviceRecoveryAttemptMade').checked ? document.getElementById('serviceRecoveryWithinTarget').checked : false;
        obj.ResolutionID = document.getElementById("resolution").options[document.getElementById("resolution").selectedIndex].value;

        jQuery.ajax({
            type: "post"
                      , url: urlUpdateServiceRecovery
                      , data: JSON.stringify(obj)
                      , contentType: "application/json; charset=UTF-8"
                      , beforeSend: function () { $('#preLoader').show(); }
                      , error: function () {
                          $('#preLoader').hide();
                      }
                      , success: function (response) {
                          loadUnsubscribeCustomer();
                          getListOfUnSubscribedCustomer();
                          loadServiceRecovery();

                          $('#preLoader').hide();
                          if (response.data.isSuccess == 0) {
                              MAIN.plugins.alertBox(0, response.data.Message);

                          } else {
                              MAIN.plugins.alertBox(1, response.data.Message);
                          }
                      }
        });
    }
}