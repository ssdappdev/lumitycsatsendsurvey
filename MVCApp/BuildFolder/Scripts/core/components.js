﻿var $ = jQuery.noConflict();
var MAIN = MAIN || {};
(function ($) {

    //*--- AJAX Request
    MAIN.ajax = {
        init: function () {
            MAIN.ajax.ajaxRequest();
        },
        ajaxRequest: function (url, data, funcSuccess, funcError) {
       
            jQuery.ajax({
                type: "post"
                , url: url
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: funcError
                , success: funcSuccess

            });

        },
        ajaxRequestNoPreloader: function (url, data, funcSuccess, funcError) {
            jQuery.ajax({
                type: "post"
                , url: url
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () {}
                , error: funcError
                , success: funcSuccess

            });
        },
        ajaxRequestNoReload: function (url, data, successMessage) {
            if (successMessage) {
                var sucessmessage = successMessage;
            } else {
                var sucessmessage = "Action executed successfully";
            }
            jQuery.ajax({
                type: "post"
                , url: url
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: ''
                , success: function (response) {
                    $('#preLoader').hide();
                    console.log('HERE:');
                    console.log(response);
                    if (response.result == true) {
                        bootbox.dialog({
                            title: "Action Result",
                            message: sucessmessage,
                            onEscape: true,
                            backdrop: true,
                            className: "bootbox-success"
                        });
                        // saveSuccess("Entry saved successfully");
                    } else {
                        bootbox.dialog({
                            title: "Error Result",
                            message: response.errors,
                            onEscape: true,
                            backdrop: true,
                            className: "bootbox-alert"
                        });
                    }
                }
            });
            function funcBeforeSend() { $('#preLoader').show(); }
        },
        //populateDropdown: function (url) {
        //    jQuery.ajax({
        //        type: "post"
        //        , url: url
        //        //, data: JSON.stringify(data)
        //        , dataType: "json"
        //        , contentType: "application/json; charset=UTF-8"
        //        , beforeSend: function () { }
        //        , error: ''
        //        , success: function (data) {
        //            $.each(data.data, function (index, value) {
        //                //alert(index);
        //                var options = "";
        //                options += "<option value=''";
        //                console.log("HERE");
        //                console.log(data);
        //            });
        //        }
        //    });
        //},
        ajaxRequestWithReload: function (url, data, successMessage, functioncallBack) {
            if (successMessage) {
                var sucessmessage = successMessage;
            } else {
                var sucessmessage = "Action executed successfully";
            }
            jQuery.ajax({
                type: "post"
                , url: url
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: ''
                , success: function (response) {
                    $('#preLoader').hide();
                    //console.log(response);
                    if (response.result == "Success") {
                        MAIN.plugins.alertBox(1, sucessmessage);
                        alert(functioncallBack);
                        setTimeout(function(){functioncallBack;},500);
                    } else {
                        MAIN.plugins.alertBox(1, response.errors);
                    }
                }
            });
            function funcBeforeSend() { $('#preLoader').show(); }
        },
        ajaxRequestWithResult: function (url, data, successMessage) {
            if (successMessage) {
                var sucessmessage = successMessage;
            } else {
                var sucessmessage = "Action executed successfully";
            }
            jQuery.ajax({
                type: "post"
                , url: url
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: function () { $('#preLoader').show(); }
                , error: ''
                , success: function (response) {
                    $('#preLoader').hide();
                    //console.log(response);
                    if (response.result == "Success") {
                        MAIN.plugins.alertBox(1, sucessmessage);
                    } else {
                        MAIN.plugins.alertBox(1, response.errors);
                    }
                }
            });
            function funcBeforeSend() { $('#preLoader').show(); }
        },
        ajaxAnnotations: function(url, data) {
            jQuery.ajax({
                type: "post"
                , url: url
                , data: JSON.stringify(data)
                , dataType: "json"
                , contentType: "application/json; charset=UTF-8"
                , beforeSend: ''
                , error: ''
                , success: function (response) {
                    if (response.errors.length > 0) {
                        bootbox.dialog({
                            title: "Error Result",
                            message: response.errors,
                            className: "bootbox-alert"
                        });
                    }

                }
            });
        },
        ajaxPartialView: function (actionUrl, data, funcSuccess, funcError) {
            jQuery.ajax({
                url: actionUrl
                , data: data
                , beforeSend: function () { $('#preLoader').show(); }
                , error: funcError
                , success: funcSuccess
                , complete: function () { $('#preLoader').hide(); }
            });
        },
        responseError: function (response) {
            bootbox.dialog({
                title: "Action Result",
                message: response.errors
            });
        }
    },

    //*--- Preloader
    MAIN.preloader = {
        close: function () {
            setTimeout(function () {
                $('body').removeClass('preloading');
                $('#preloader').removeClass("fadeInUp").addClass("fadeOut");
            }, 1000);
        }
    };

    //*--- NAVIGATION
    MAIN.navigation = {
        init: function () {
            MAIN.navigation.mobileMenu();
            MAIN.navigation.currentUser();
        },
        mobileMenu: function () {
            $('.navbar-toggle').click(function () {
                $('.main-nav').toggle();
                $('.sub-nav').removeClass('slide-in');
            })
            $('.main-nav > li > a').click(function () {
                //event.preventDefault();
                $(this).siblings('.sub-nav').toggleClass('slide-in');
            });
            $('.mobile-top-link button').click(function () {
                $(this).parents('.sub-nav').toggleClass('slide-out');
            });
        },
        currentUser: function () {
            $('#userName').text($('#currentUserName').val());
        }
    };

    //*--- FOOTER
    MAIN.footer = {
        init: function () {
            MAIN.footer.yearStamp();
        },
        yearStamp: function () {
            var d = new Date();
            var year = d.getYear();
            $('#yearStamp').text(year + 1900);
        }
    };

    //*--- COMPONENTS
    MAIN.components = {
        init: function () {
            MAIN.components.C016();
        },
        C016: function () {
            /* C016 - TABS Jquery*/
            /* 1.) Must set a selected tab by default, in this case Tab 1 is selected */
            /* 2.) Instantiate C016 Tab on document ready */
            /* NOTE: Every anchor must have data attribute ex: data-tab="tabComponent1" as this will serve as the call for active tab */
            var activeC016_item = $('.C016-horizontal-tabs .nav li a.selected').attr("data-tab");
            $(".tab-content").hide();
            $("#" + activeC016_item).show();

            /* 3.) Tabs switch content by clicking, adding class to the clicked tab and removing class to the old tab */
            /* 4.) This code also hide All tabs and show the corresponding Tab of the Clicked Anchor Tab */
            /* NOTE: every tab component must have an ID similar to this: id="tabComponent1", id="tabComponent2" and etc. */
            $('.C016-horizontal-tabs .nav li a').on("click", function () {
                event.preventDefault();
                $(this).addClass("selected");
                $(this).parents().siblings().children("a").removeClass("selected");
                var activeC016_item = $('.C016-horizontal-tabs .nav li a.selected').attr("data-tab");
                $(this).parents('.tabs-header').siblings('.tab-content').hide();
                $("#" + activeC016_item).show();
            });
        },
        JN_Scroll: function () {
            // Get container scroll position
            var fromTop = $(this).scrollTop() + topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function () {
                if ($(this).offset().top < fromTop)
                    return this;
            });
            // Get the id of the current element
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id) {
                lastId = id;
                // Set/remove active class
                menuItems
                  .parent().removeClass("active")
                  .end().filter("[href='#" + id + "']").parent().addClass("active");
            }
        }

    };

    MAIN.ImageResponsive = {

        init: function () {
            MAIN.ImageResponsive.HComponents();
        },
        HComponents: function () {
            var WindowWidth = $(window).width();
            if ((WindowWidth <= 768)) {

                $('.H003-mini-hero img, .H002-hero img, .VN015-featured-navigation-banners img, .C010-big-feature-image-left img').attr('src', function (index, src) {
                    return src.replace(".jpg", "_768.jpg");
                });
            }
        }
    };

    MAIN.plugins = {
        init: function () {
            MAIN.plugins.datePicker();
            MAIN.plugins.selectize();
        },

        alertBox: function (status, message) {
            if (status == 1) {
                _class = "alert-success";
            } else if (status == 0) {
                _class = "alert-danger";
            } else {
                _class = "alert-light";
            }
            var alertBox = "";
            alertBox += "<div class='alert "+_class+" alert-dismissible '>";
            alertBox += "   <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
            alertBox += "   <span>"+message+"</span>";
            alertBox += "</div>";
            $('#alertBox').html(alertBox);
            $('#alertBox .alert').show();

            setTimeout(function () {
               // alert('test');
                $('#alertBox .alert').toggle();
            }, 5000);
        },
        datePicker: function () {
            $('.dateTimePicker').datepicker({
                changeMonth: true,
                changeYear: true,
                showOn: "button",
                buttonImage: "../Content/images/calendar.gif",
                buttonImageOnly: true,
                buttonText: "Select date",
                dateFormat: 'mm/dd/yy',
            });
        },
        dataTable: function (actionUrl, tableID, columns, orderArray, ID) {
            var ordernum;
            if (orderArray) {
                ordernum = orderArray;
            } else {
                ordernum = 0;
            }
            //DATA TABLE
            var rowId;
            if (ID) {
                rowId = ID;
            } else {
                rowId = "Id"
            }
 
            var table = $(tableID).DataTable({
                processing: true,
                ajax: {
                    url: actionUrl
                },
                "initComplete": function () { $('#preLoader').hide(); },
                "stripeClasses": ['odd', 'even'],
                rowId: rowId,
                columns: columns,
                "order": [[ordernum, "desc"]],
                orderCellsTop: true,
                fixedHeader: true,
                //ordering: false,
                filtering: false,
                pageLength: 20,
                "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
            });
            // Setup - add a text input to each footer cell
            $(tableID + ' thead tr').clone(true).addClass('searchFilter').appendTo(tableID + ' thead').hide();
            $(tableID + ' thead tr:eq(1) th').each(function (i) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });
            var filterButton = "<a class='btn btn-secondary btn-arrow filterbutton'><span><span class='text-wrapper'>Show Filters</span></span></a>";
            $(tableID).siblings('.dataTables_length').append(filterButton);
            $(tableID + '_filter').hide();
            $('input[placeholder="Search Action"]').hide()
            $(tableID).siblings().children('.filterbutton').click(function () { $(tableID + ' .searchFilter').toggle(); });
        },
        dataTableNoFilters: function (actionUrl, tableID, columns, ID) {
            //DATA TABLE
            var rowId;
            if (ID) {
                rowId = ID;
            } else {
                rowId = "Id"
            }
           
            var table = $(tableID).DataTable({
                processing: true,
                ajax: {
                    url: actionUrl,
                },
                "initComplete": function () { $('#preLoader').hide(); },
                "stripeClasses": ['odd', 'even'],
                rowId: rowId,
                columns: columns,
                orderCellsTop: true,
                fixedHeader: true,
                ordering: false,
                filtering: false,
                pageLength: 20,
                "lengthChange": false,
                "searching": false
            });
            table.ajax.reload();
        },
        dataTablesAjaxReload: function (table) {
            var tableReload = $(table).DataTable();
            $(tableReload).ajax.reload();
        },
        selectize: function () {
            $('.selectize').selectize({
                placeholder: "-- Please Select --"
            });
        },
        bootbox: function (message, title) {
            if (title == undefined) {
                _title = "Action Result";
            } else {
                _title = title;
            }
            bootbox.dialog({
                title:  _title,
                message: message,
                onEscape: true,
                backdrop: true,
                className: "bootbox-success"
            });
        },
        bootboxReload: function (message, title) {
            if (title == undefined) {
                _title = "Action Result";
            } else {
                _title = title;
            }
            bootbox.dialog({
                closeButton: false,
                title: _title,
                message: message + " window will refresh in 5 seconds",
                onEscape: true,
                backdrop: true,
                className: "bootbox-success"
            });
            setTimeout(function () {
                location.reload();
            }, 5000);
        }
    };

    MAIN.validate = {
        init: function () {
            //MAIN.validate.requiredFields();
        },
        requiredFields: function (formID) {
            var totalRequiredFields = $(formID + " :input[required]").length;
            var validated;
            var validatedFields = 0;
            $(formID + " :input[required]").each(function () {
                if (
                    $(this).is('input[type=text]') ||
                    $(this).is('input[type=number]') ||
                    $(this).is('input[type=password]') ||
                    $(this).is('input[type=email]')
                    ) {
                    if ($(this).val() != '') {
                        validatedFields++;
                        $(this).removeClass('required');
                        $(this).parents('.form-group').children('label').removeClass('required');
                        $(this).parents('.form-group').removeClass('required');
                    } else {
                        $(this).addClass('required');
                        $(this).parents('.form-group').children('label').addClass('required');
                        $(this).parents('.form-group').addClass('required');
                    }
                }
                if ($(this).is('select')) {
                    if ($(this).val() != '0') {
                        validatedFields++;
                        $(this).removeClass('required');
                        $(this).parents('.form-group').children('label').removeClass('required');
                    } else {
                        $(this).parents('.form-group').children('label').addClass('required');
                        $(this).addClass('required');
                    }
                }
                if ($(this).is('textarea')) {
                    if ($(this).val().length != 0) {
                        validatedFields++;
                        $(this).removeClass('required');
                        $(this).parents('.form-group').children('label').removeClass('required');
                    } else {
                        $(this).addClass('required');
                        $(this).parents('.form-group').children('label').addClass('required');
                    }
                }
            });
            if (validatedFields == totalRequiredFields) {
                $('.requiredMessage').hide();
                $('#formValidated').val(1);
            } else {
                $('#formValidated').val(0);
                $('.requiredMessage').show();
            }
        }
    }
    /*--- DOCUMENT ON READY ---*/
    MAIN.documentOnReady = {
        init: function () {
           // MAIN.plugins.alertBox();
            MAIN.ajax.init();
            MAIN.navigation.init();
            MAIN.footer.init();
            MAIN.plugins.init();
            MAIN.validate.init();
            MAIN.components.init();
            // MAIN.ImageResponsive.init();
        }
    };

    /*--- WINDOW SCROLL ---*/
    MAIN.windowOnScroll = {
        init: function () {

        }
    };



    //*--- END Function
})(jQuery);

//* Instantiate all functions declared on MAIN.documentOnReady.init
$(document).ready(MAIN.documentOnReady.init);

//* Instantiate all functions declared on MAIN.windowOnScroll.init
$(window).scroll(MAIN.windowOnScroll.init);


/* --- JUMP NAVIGATION ---*/
// Cache selectors
var lastId,
    topMenu = $("#jump-nav"),
    topMenuHeight = topMenu.outerHeight() + 15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function (e) {
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
    $('html, body').stop().animate({
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
});


// Bind to scroll
$(window).scroll(function () {


    // Get container scroll position
    var fromTop = $(this).scrollTop() + topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function () {
        if ($(this).offset().top < fromTop)
            return this;
    });
    // Get the id of the current element
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : "";

    if (lastId !== id) {
        lastId = id;
        // Set/remove active class
        menuItems
          .parent().removeClass("active")
          .end().filter("[href='#" + id + "']").parent().addClass("active");
    }
});